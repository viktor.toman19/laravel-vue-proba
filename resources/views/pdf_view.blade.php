<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        * {
            font-family: "DejaVu Sans Mono", monospace;
            font-size: 12px;
        }

        .table {
            width: 100%;
            border: 1px solid black;
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .text-center {
            text-align: center;
        }

        .p-3 {
            padding: 1rem;
        }
    </style>
    <title>PDF</title>
</head>
<body>
<table class="table">
    <tr class="text-center">
        <td colspan="4">Tűzoltó készülék</td>
        <td colspan="1">Gyártás</td>
        <td colspan="4">Ellenörzések időpontja</td>
        <td colspan="1">Karbantartás időpontja</td>
        <td colspan="1">Eszköz megj.</td>
    </tr>
    <tr class="text-center">
        <td>Ssz</td>
        <td>Gyári szám</td>
        <td>Készenléti helye</td>
        <td>Típusa</td>
        <td></td>
        <td>I. n.év</td>
        <td>II. n.év</td>
        <td>III. n.év</td>
        <td>IV. n.év</td>
        <td></td>
        <td></td>
    </tr>
    <tbody>
    @foreach($data as $extinguisher)

        <tr>
            <td class="text-center">
                <div class="p-3">
                    {{$extinguisher['id']}}
                </div>
            </td>
            <td class="text-center">
                <div class="p-3">
                    {{ $extinguisher['serialNumber'] }}
                </div>
            </td>
            <td class="text-center">
                <div class="p-3">
                    {{ $extinguisher['location'] }}
                </div>
            </td>
            <td>
                <div class="p-3">
                    {{ $extinguisher['extinguisherTypeText'] }}
                </div>
            </td>
            <td class="text-center">
                <div class="p-3">
                    {{ $extinguisher['productionTimeStr'] }}
                </div>
            </td>
            <td class="text-center">
                @if($extinguisher['firstMaintenance'] && $extinguisher['firstMaintenance']['maintenanceTime'])
                    <div class="border border-secondary p-3 text-secondary text-nowrap m-auto">
                        {{ $extinguisher['firstMaintenance']['maintenanceTime'] }}
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($extinguisher['secondMaintenance'] && $extinguisher['secondMaintenance']['maintenanceTime'])
                    <div class="border border-secondary p-3 text-secondary text-nowrap m-auto">
                        {{ $extinguisher['secondMaintenance']['maintenanceTime'] }}
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($extinguisher['thirdMaintenance'] && $extinguisher['thirdMaintenance']['maintenanceTime'])
                    <div class="border border-secondary p-3 text-secondary text-nowrap m-auto">
                        {{ $extinguisher['thirdMaintenance']['maintenanceTime'] }}
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($extinguisher['fourthMaintenance'] && $extinguisher['fourthMaintenance']['maintenanceTime'])
                    <div class="border border-secondary p-3 text-secondary text-nowrap m-auto">
                        {{ $extinguisher['fourthMaintenance']['maintenanceTime'] }}
                    </div>
                @endif
            </td>
            <td class="text-center">
                @if($extinguisher['basicMaintenance'] && $extinguisher['basicMaintenance']['maintenanceTime'])
                    <div class="border border-secondary p-3 text-secondary text-nowrap m-auto">
                        {{ $extinguisher['basicMaintenance']['maintenanceTime'] }}
                    </div>
                @endif
            </td>
            <td class="text-center">{{ $extinguisher['note'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>

