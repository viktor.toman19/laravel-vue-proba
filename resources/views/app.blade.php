<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel & Vue</title>

    @vite('resources/sass/app.scss')
</head>
<body class="bg-light">
<div id="app"></div>

@vite('resources/js/app.js')
</body>
</html>
