import './bootstrap';
import {createApp} from 'vue'
import App from './components/App.vue'
import router from "./routes";
import VueAxios from 'vue-axios';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css"
import PrimeVue from 'primevue/config';
import DialogService from 'primevue/dialogservice';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';

createApp(App)
    .use(PrimeVue)
    .use(DialogService)
    .use(ConfirmationService)
    .use(ToastService)
    .use(router)
    .use(VueAxios, axios)
    .mount("#app")
