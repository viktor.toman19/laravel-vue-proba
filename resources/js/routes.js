import {createRouter, createWebHistory} from "vue-router";
const ExtinguisherList = () => import('./components/extinguisher/List.vue')
export const routes = [
    {
        path: '/',
        redirect: {
            path: '/extinguisher'
        }
    },
    {
        name: 'extinguisherList',
        path: '/extinguisher',
        component: ExtinguisherList
    }
]

const router = new createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
});

export default router;
