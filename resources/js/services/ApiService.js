import axios from "axios";

export class ApiService {
    async getAllExtinguishers() {
        return await axios.get('/api/extinguisher')
    }
    async getAllExtinguisherTypes() {
        return await axios.get('/api/extinguisher-types')
    }

    async saveExtinguisher(data) {
        return await axios.post('/api/extinguisher', data)
    }

    async updateExtinguisher(data) {
        return await axios.put('/api/extinguisher/' + data.id, data)
    }

    async saveMaintenance(data) {
        return await axios.post('/api/extinguisher/save-maintenance/' + data.extinguisherId, data)
    }

    async updateMaintenance(data) {
        return await axios.put('/api/extinguisher/update-maintenance/' + data.extinguisherId, data)
    }

    async deleteMaintenance(data) {
        return await axios.delete('/api/extinguisher/delete-maintenance/' + data.id)
    }

    async createDocument(data) {
        return await axios.get('/api/extinguisher-document')
    }
}
