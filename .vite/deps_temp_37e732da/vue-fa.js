import {
  Fragment,
  computed,
  createBaseVNode,
  createCommentVNode,
  createElementBlock,
  defineComponent,
  normalizeClass,
  normalizeStyle,
  openBlock,
  popScopeId,
  pushScopeId,
  renderSlot,
  vShow,
  withDirectives
} from "./chunk-YNNA4QFB.js";
import "./chunk-WVZ3F7T3.js";

// node_modules/vue-fa/dist/vue-fa.esm.js
function _extends() {
  _extends = Object.assign || function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  return _extends.apply(this, arguments);
}
var validFlip = [
  "horizontal",
  "vertical",
  "both"
];
var validPull = [
  "right",
  "left"
];
var typeNumber = {
  type: [
    Number,
    String
  ],
  validator: (value) => /^[-\d.]+$/.test(`${value}`)
};
var parseNumber = (num) => parseFloat(num);
function getStyles(size, pull, fw) {
  let float;
  let width;
  const height = "1em";
  let lineHeight;
  let fontSize;
  let textAlign;
  let verticalAlign = "-.125em";
  const overflow = "visible";
  if (fw) {
    textAlign = "center";
    width = "1.25em";
  }
  if (pull) {
    float = pull;
  }
  if (size) {
    if (size === "lg") {
      fontSize = "1.33333em";
      lineHeight = ".75em";
      verticalAlign = "-.225em";
    } else if (size === "xs") {
      fontSize = ".75em";
    } else if (size === "sm") {
      fontSize = ".875em";
    } else {
      fontSize = size.replace("x", "em");
    }
  }
  return {
    float,
    width,
    height,
    "line-height": lineHeight,
    "font-size": fontSize,
    "text-align": textAlign,
    "vertical-align": verticalAlign,
    "transform-origin": "center",
    overflow
  };
}
function getTransform(scale, translateX, translateY, rotate, flip, translateTimes = 1, translateUnit = "", rotateUnit = "") {
  let flipX = 1;
  let flipY = 1;
  if (flip) {
    if (flip === "horizontal") {
      flipX = -1;
    } else if (flip === "vertical") {
      flipY = -1;
    } else {
      flipX = flipY = -1;
    }
  }
  return [
    `translate(${parseNumber(translateX) * translateTimes}${translateUnit},${parseNumber(translateY) * translateTimes}${translateUnit})`,
    `scale(${flipX * parseNumber(scale)},${flipY * parseNumber(scale)})`,
    rotate && `rotate(${rotate}${rotateUnit})`
  ].join(" ");
}
var script$2 = defineComponent({
  props: {
    icon: {
      type: Object,
      required: true
    },
    size: {
      type: String,
      validator: function validator(value) {
        return /^(lg|xs|sm|([\d.]+)x)$/.test(value);
      }
    },
    color: String,
    fw: Boolean,
    pull: {
      type: String,
      validator: function validator2(value) {
        return validPull.indexOf(value) >= 0;
      }
    },
    scale: _extends({}, typeNumber, {
      default: 1
    }),
    translateX: _extends({}, typeNumber, {
      default: 0
    }),
    translateY: _extends({}, typeNumber, {
      default: 0
    }),
    flip: {
      type: String,
      validator: function validator3(value) {
        return validFlip.indexOf(value) >= 0;
      }
    },
    rotate: typeNumber,
    spin: Boolean,
    pulse: Boolean,
    primaryColor: String,
    secondaryColor: String,
    primaryOpacity: {
      type: [Number, String],
      default: 1
    },
    secondaryOpacity: {
      type: [Number, String],
      default: 0.4
    },
    swapOpacity: Boolean
  },
  setup: function setup(props) {
    var i = function i2() {
      var _props$icon$icon, _props$icon;
      return (_props$icon$icon = (_props$icon = props.icon) == null ? void 0 : _props$icon.icon) != null ? _props$icon$icon : [0, 0, "", [], ""];
    };
    return {
      i: computed(i),
      style: computed(function() {
        if (!i()[4]) {
          return {};
        }
        return getStyles(props.size, props.pull, props.fw);
      }),
      transform: computed(function() {
        return getTransform(props.scale, props.translateX, props.translateY, props.rotate, props.flip, 512);
      })
    };
  }
});
pushScopeId("data-v-7e44f4d4");
var _hoisted_1$1 = ["viewBox"];
var _hoisted_2 = ["transform", "transform-origin"];
var _hoisted_3 = ["transform"];
var _hoisted_4 = ["d", "fill", "transform"];
var _hoisted_5 = ["d", "fill", "fill-opacity", "transform"];
var _hoisted_6 = ["d", "fill", "fill-opacity", "transform"];
popScopeId();
function render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return withDirectives((openBlock(), createElementBlock(
    "svg",
    {
      viewBox: "0 0 " + _ctx.i[0] + " " + _ctx.i[1],
      class: normalizeClass({
        "vue-fa": true,
        spin: _ctx.spin,
        pulse: _ctx.pulse
      }),
      style: normalizeStyle(_ctx.style),
      "aria-hidden": "true",
      role: "img",
      xmlns: "http://www.w3.org/2000/svg"
    },
    [_ctx.i[4] ? (openBlock(), createElementBlock(
      "g",
      {
        key: 0,
        transform: "translate(" + _ctx.i[0] / 2 + " " + _ctx.i[1] / 2 + ")",
        "transform-origin": _ctx.i[0] / 4 + " 0"
      },
      [createBaseVNode(
        "g",
        {
          transform: _ctx.transform
        },
        [typeof _ctx.i[4] === "string" ? (openBlock(), createElementBlock(
          "path",
          {
            key: 0,
            d: _ctx.i[4],
            fill: _ctx.color || _ctx.primaryColor || "currentColor",
            transform: "translate(" + _ctx.i[0] / -2 + " " + _ctx.i[1] / -2 + ")"
          },
          null,
          8,
          _hoisted_4
        )) : (openBlock(), createElementBlock(
          Fragment,
          {
            key: 1
          },
          [createBaseVNode(
            "path",
            {
              d: _ctx.i[4][0],
              fill: _ctx.secondaryColor || _ctx.color || "currentColor",
              "fill-opacity": _ctx.swapOpacity !== false ? _ctx.primaryOpacity : _ctx.secondaryOpacity,
              transform: "translate(" + _ctx.i[0] / -2 + " " + _ctx.i[1] / -2 + ")"
            },
            null,
            8,
            _hoisted_5
          ), createBaseVNode(
            "path",
            {
              d: _ctx.i[4][1],
              fill: _ctx.primaryColor || _ctx.color || "currentColor",
              "fill-opacity": _ctx.swapOpacity !== false ? _ctx.secondaryOpacity : _ctx.primaryOpacity,
              transform: "translate(" + _ctx.i[0] / -2 + " " + _ctx.i[1] / -2 + ")"
            },
            null,
            8,
            _hoisted_6
          )],
          64
          /* STABLE_FRAGMENT */
        ))],
        8,
        _hoisted_3
      )],
      8,
      _hoisted_2
    )) : createCommentVNode("v-if", true)],
    14,
    _hoisted_1$1
  )), [[vShow, _ctx.i[4]]]);
}
function styleInject(css, ref) {
  if (ref === void 0)
    ref = {};
  var insertAt = ref.insertAt;
  if (!css || typeof document === "undefined") {
    return;
  }
  var head = document.head || document.getElementsByTagName("head")[0];
  var style = document.createElement("style");
  style.type = "text/css";
  if (insertAt === "top") {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }
  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}
var css_248z$1 = "@-webkit-keyframes spin-7e44f4d4{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin-7e44f4d4{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.spin[data-v-7e44f4d4]{-webkit-animation:spin-7e44f4d4 2s 0s infinite linear;animation:spin-7e44f4d4 2s 0s infinite linear}.pulse[data-v-7e44f4d4]{-webkit-animation:spin-7e44f4d4 1s infinite steps(8);animation:spin-7e44f4d4 1s infinite steps(8)}";
styleInject(css_248z$1);
script$2.render = render$2;
script$2.__scopeId = "data-v-7e44f4d4";
script$2.__file = "src/fa.vue";
var script$1 = defineComponent({
  props: {
    size: {
      type: String,
      validator: function validator4(value) {
        return /^(lg|xs|sm|([\d.]+)x)$/.test(value);
      }
    },
    pull: {
      type: String,
      validator: function validator5(value) {
        return validPull.indexOf(value) >= 0;
      }
    }
  },
  setup: function setup2(props) {
    return {
      style: computed(function() {
        return getStyles(props.size, props.pull, true);
      })
    };
  }
});
function render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    "span",
    {
      class: "vue-fa-layers",
      style: normalizeStyle(_ctx.style)
    },
    [renderSlot(_ctx.$slots, "default")],
    4
    /* STYLE */
  );
}
var css_248z = ".vue-fa-layers[data-v-62be850b]{display:inline-block;position:relative}.vue-fa-layers[data-v-62be850b] .vue-fa{position:absolute;bottom:0;left:0;right:0;top:0;margin:auto;text-align:center}.vue-fa-layers[data-v-62be850b] .vue-fa-layers-text{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.vue-fa-layers[data-v-62be850b] .vue-fa-layers-text span{display:inline-block}";
styleInject(css_248z);
script$1.render = render$1;
script$1.__scopeId = "data-v-62be850b";
script$1.__file = "src/fa-layers.vue";
var script = defineComponent({
  props: {
    size: {
      type: String,
      validator: function validator6(value) {
        return /^(lg|xs|sm|([\d.]+)x)$/.test(value);
      }
    },
    color: String,
    scale: _extends({}, typeNumber, {
      default: 1
    }),
    translateX: _extends({}, typeNumber, {
      default: 0
    }),
    translateY: _extends({}, typeNumber, {
      default: 0
    }),
    flip: {
      type: String,
      validator: function validator7(value) {
        return validFlip.indexOf(value) >= 0;
      }
    },
    rotate: typeNumber
  },
  setup: function setup3(props) {
    return {
      style: computed(function() {
        return _extends({}, getStyles(props.size), {
          color: props.color,
          transform: getTransform(props.scale, props.translateX, props.translateY, props.rotate, props.flip, void 0, "em", "deg")
        });
      })
    };
  }
});
var _hoisted_1 = {
  class: "vue-fa-layers-text"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("span", _hoisted_1, [createBaseVNode(
    "span",
    {
      style: normalizeStyle(_ctx.style)
    },
    [renderSlot(_ctx.$slots, "default")],
    4
    /* STYLE */
  )]);
}
script.render = render;
script.__file = "src/fa-layers-text.vue";
export {
  script$2 as Fa,
  script$1 as FaLayers,
  script as FaLayersText,
  script$2 as default
};
//# sourceMappingURL=vue-fa.js.map
