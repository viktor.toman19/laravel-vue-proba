import {
  primebus
} from "./chunk-XADPZ4M6.js";
import {
  markRaw
} from "./chunk-YNNA4QFB.js";
import "./chunk-WVZ3F7T3.js";

// node_modules/primevue/usedialog/usedialog.esm.js
var PrimeVueDialogSymbol = Symbol();

// node_modules/primevue/dynamicdialogeventbus/dynamicdialogeventbus.esm.js
var DynamicDialogEventBus = primebus();

// node_modules/primevue/dialogservice/dialogservice.esm.js
var DialogService = {
  install: (app) => {
    const DialogService2 = {
      open: (content, options) => {
        const instance = {
          content: content && markRaw(content),
          options: options || {},
          data: options && options.data,
          close: (params) => {
            DynamicDialogEventBus.emit("close", { instance, params });
          }
        };
        DynamicDialogEventBus.emit("open", { instance });
        return instance;
      }
    };
    app.config.unwrapInjectedRef = true;
    app.config.globalProperties.$dialog = DialogService2;
    app.provide(PrimeVueDialogSymbol, DialogService2);
  }
};
export {
  DialogService as default
};
//# sourceMappingURL=primevue_dialogservice.js.map
