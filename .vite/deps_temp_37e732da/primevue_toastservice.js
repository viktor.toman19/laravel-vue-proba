import {
  ToastEventBus
} from "./chunk-V6DHOEU5.js";
import "./chunk-XADPZ4M6.js";
import "./chunk-YNNA4QFB.js";
import "./chunk-WVZ3F7T3.js";

// node_modules/primevue/usetoast/usetoast.esm.js
var PrimeVueToastSymbol = Symbol();

// node_modules/primevue/toastservice/toastservice.esm.js
var ToastService = {
  install: (app) => {
    const ToastService2 = {
      add: (message) => {
        ToastEventBus.emit("add", message);
      },
      removeGroup: (group) => {
        ToastEventBus.emit("remove-group", group);
      },
      removeAllGroups: () => {
        ToastEventBus.emit("remove-all-groups");
      }
    };
    app.config.globalProperties.$toast = ToastService2;
    app.provide(PrimeVueToastSymbol, ToastService2);
  }
};
export {
  ToastService as default
};
//# sourceMappingURL=primevue_toastservice.js.map
