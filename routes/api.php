<?php

use App\Http\Controllers\ExtinguisherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('api')->group(function () {
    Route::resource('extinguisher', ExtinguisherController::class);

    Route::get('extinguisher-types', [ExtinguisherController::class, 'getExtinguisherTypes'])->name('extinguisher-types');
    Route::get('extinguisher-document', [ExtinguisherController::class, 'getExtinguisherDocument'])->name('extinguisher-document');

    Route::post('extinguisher/save-maintenance/{extinguisher}', [ExtinguisherController::class, 'saveMaintenance'])
        ->name('save-maintenance');

    Route::put('extinguisher/update-maintenance/{extinguisher}', [ExtinguisherController::class, 'updateMaintenance'])
        ->name('update-maintenance');

    Route::delete('extinguisher/delete-maintenance/{maintenance}', [ExtinguisherController::class, 'destroyMaintenance'])
        ->name('destroy-maintenance');
});
