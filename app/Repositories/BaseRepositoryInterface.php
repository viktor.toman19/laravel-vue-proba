<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function find($id);
    public function all();
    public function create(array $data);
    public function update($model, array $data);
    public function destroy($model);
}
