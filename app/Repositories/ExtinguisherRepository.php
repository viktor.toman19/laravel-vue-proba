<?php

namespace App\Repositories;

use App\Http\Resources\ExtinguisherResource;
use App\Http\Resources\MaintenanceResource;
use App\Models\Extinguisher;
use App\Models\Maintenance;
use Carbon\Carbon;

class ExtinguisherRepository implements BaseRepositoryInterface
{
    public function __construct(
        protected Extinguisher $model,
        protected Maintenance $maintenance,
    ) {
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function all()
    {
        return $this->model->orderBy('created_at', 'DESC')->get();
    }

    public function pdfResponse(): array
    {
        $response = [];

        foreach ($this->all() as $extinguisher) {
            $productionTime = Carbon::parse($extinguisher->production_time);
            $basicMaintenance = $extinguisher->getBasicMaintenance();
            $firstMaintenance = $extinguisher->getFirstMaintenance();
            $secondMaintenance = $extinguisher->getSecondMaintenance();
            $thirdMaintenance = $extinguisher->getThirdMaintenance();
            $fourthMaintenance = $extinguisher->getFourthMaintenance();

            $response[] = [
                'id' => $extinguisher->id,
                'identifier' => $extinguisher->identifier,
                'premises' => $extinguisher->premises,
                'location' => $extinguisher->location,
                'extinguisherType' => $extinguisher->extinguisher_type,
                'extinguisherTypeText' => Extinguisher::TYPES[$extinguisher->extinguisher_type]['value'],
                'serialNumber' => $extinguisher->serial_number,
                'productionTimeStr' => $productionTime->format('Y.m.d.'),
                'note' => $extinguisher->note ?: '',
                'basicMaintenance' => $basicMaintenance ? [
                    'maintenanceTime' => (Carbon::parse($basicMaintenance->maintenance_time)->format('m.d.')) . ($basicMaintenance->getMaintenanceType() === 2 ? ' Ü' : ' A')
                ] : null,
                'firstMaintenance' => $firstMaintenance ? [
                    'maintenanceTime' => (Carbon::parse($firstMaintenance->maintenance_time)->format('m.d.')) . ($firstMaintenance->getMaintenanceType() === 2 ? ' Ü' : ' A')
                ] : null,
                'secondMaintenance' => $secondMaintenance ? [
                    'maintenanceTime' => (Carbon::parse($secondMaintenance->maintenance_time)->format('m.d.')) . ($secondMaintenance->getMaintenanceType() === 2 ? ' Ü' : ' A')
                ] : null,
                'thirdMaintenance' => $thirdMaintenance ? [
                    'maintenanceTime' => (Carbon::parse($thirdMaintenance->maintenance_time)->format('m.d.')) . ($thirdMaintenance->getMaintenanceType() === 2 ? ' Ü' : ' A')
                ] : null,
                'fourthMaintenance' => $fourthMaintenance ? [
                    'maintenanceTime' => (Carbon::parse($fourthMaintenance->maintenance_time)->format('m.d.')) . ($fourthMaintenance->getMaintenanceType() === 2 ? ' Ü' : ' A')
                ] : null,
            ];
        }

        return $response;
    }

    public function create(array $data)
    {
        $model = null;
        $productionTime = $data['productionTime'];
        $year = $productionTime['year'];
        $month = $productionTime['month'];
        $day = $productionTime['day'];

        $clone = $data['clone'] ?? 1;

        for ($x = 0; $x < $clone; $x++) {
            $model = new $this->model;

            $model->premises = (int) $data['premises'];
            $model->identifier = $data['identifier'];
            $model->location = $data['location'];
            $model->extinguisher_type = (int) $data['extinguisherType'];
            $model->serial_number = $data['serialNumber'];
            $model->production_time = Carbon::createFromFormat('Y-m-d', "$year-$month-$day");
            $model->note = $data['note'] ?: null;

            $model->save();
            $model->fresh();
        }


        return $model;
    }

    public function update($model, $data)
    {
        $productionTime = $data['productionTime'];
        $year = $productionTime['year'];
        $month = $productionTime['month'];
        $day = $productionTime['day'];

        $model->update([
            'premises' => (int) $data['premises'],
            'identifier' => $data['identifier'],
            'location' => $data['location'],
            'extinguisher_type' => (int) $data['extinguisherType'],
            'serial_number' => $data['serialNumber'],
            'production_time' => Carbon::createFromFormat('Y-m-d', "$year-$month-$day"),
            'note' => $data['note'] ?: null
        ]);

        return $model;
    }

    public function destroy($model)
    {
        return $model->delete();
    }

    public function saveMaintenance(Extinguisher $extinguisher, array $data)
    {
        $period = null;
        $model = new $this->maintenance;

        if (!$extinguisher->hasMaintenance()) {
            $period = 0;
        } elseif ($extinguisher->getBasicMaintenance()) {
            $period = 1;
        }

        if ($extinguisher->getFirstMaintenance()) {
            $period = 2;
        }

        if ($extinguisher->getSecondMaintenance()) {
            $period = 3;
        }

        if ($extinguisher->getThirdMaintenance()) {
            $period = 4;
        }

        $model->maintenance_type = $extinguisher->hasBasicMaintenanceType() ? Maintenance::TYPE_OPERATORS : Maintenance::TYPE_BASIC;
        $model->maintenance_period = $period;
        $model->maintenance_time = Carbon::createFromFormat('Y-m-d', $data['maintenanceDate']);
        $model->note = $data['note'] ?: null;

        $model->extinguisher()->associate($extinguisher);

        $model->save();
        $model->fresh();

        return $model;
    }

    public function updateMaintenance(Extinguisher $extinguisher, array $data)
    {
        $model = Maintenance::whereId($data['id'])->first();

        $model->maintenance_time = Carbon::createFromFormat('Y-m-d', $data['maintenanceDate']);
        $model->note = $data['note'] ?: null;

        $model->extinguisher()->associate($extinguisher);

        $model->save();
        $model->fresh();

        return $model;
    }
}
