<?php

namespace App\Http\Controllers;

use App\Http\Requests\MaintenanceRequest;
use App\Http\Resources\ExtinguisherResource;
use App\Models\Extinguisher;
use App\Models\Maintenance;
use App\Repositories\ExtinguisherRepository;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\ExtinguisherRequest;


class ExtinguisherController extends Controller
{
    public function __construct(private readonly ExtinguisherRepository $repository)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $extinguishers = $this->repository->all();

        return response()->json(
            ExtinguisherResource::collection($extinguishers),
            Response::HTTP_OK
        );
    }

    public function getExtinguisherTypes(): JsonResponse
    {
        return response()->json(
            Extinguisher::TYPES,
            Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ExtinguisherRequest $request): JsonResponse
    {
        $this->repository->create($request->all());

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Extinguisher $extinguisher): JsonResponse
    {
        return response()->json(
            new ExtinguisherResource($extinguisher),
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Extinguisher $extinguisher, ExtinguisherRequest $request): JsonResponse
    {
        $extinguisher = $this->repository->update($extinguisher, $request->all());

        return response()->json([
            'data' => new ExtinguisherResource($extinguisher)
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Extinguisher $extinguisher): JsonResponse
    {
        $this->repository->destroy($extinguisher);

        return response()->json([], Response::HTTP_OK);
    }

    public function saveMaintenance(Extinguisher $extinguisher, MaintenanceRequest $request): JsonResponse
    {
        $this->repository->saveMaintenance($extinguisher, $request->all());

        return response()->json([
            'data' => new ExtinguisherResource($extinguisher)
        ], Response::HTTP_CREATED);
    }

    public function updateMaintenance(Extinguisher $extinguisher, MaintenanceRequest $request): JsonResponse
    {
        $this->repository->updateMaintenance($extinguisher, $request->all());

        return response()->json([
            'data' => new ExtinguisherResource($extinguisher)
        ], Response::HTTP_OK);
    }

    public function destroyMaintenance(Maintenance $maintenance): JsonResponse
    {
        $this->repository->destroy($maintenance);

        return response()->json([], Response::HTTP_OK);
    }

    public function getExtinguisherDocument(): JsonResponse
    {
        $path = public_path('pdf/');

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $extinguishers = $this->repository->pdfResponse();
        $pdfFile = 'pdf/probafeladat-naplo-' . Str::random(30) . '.pdf';

        PDF::loadView('pdf_view', [
            'data' => $extinguishers
        ])->setPaper('A3')->save($pdfFile);

        return response()->json(
            ['url' => url($pdfFile)],
            Response::HTTP_OK
        );
    }
}
