<?php

namespace App\Http\Resources;

use App\Models\Maintenance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MaintenanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        if ($this->maintenance_period === 1) {
            $period = 'I. n.év';
        } elseif($this->maintenance_period === 2) {
            $period = 'II. n.év';
        } elseif($this->maintenance_period === 3) {
            $period = 'III. n.év';
        } elseif($this->maintenance_period === 4) {
            $period = 'IV. n.év';
        }

        return [
            'id' => $this->id,
            'extinguisherId' => $this->extinguisher()->first()->id,
            'maintenanceType' => $this->maintenance_type,
            'maintenanceTypeText' => $this->maintenance_type === Maintenance::TYPE_BASIC ? 'Alapkarbantartás' : 'Üzemeltetői ellenörzés (' . $period . ')',
            'maintenanceTypeRef' => $this->getMaintenanceType(),
            'maintenancePeriod' => $this->maintenance_period,
            'maintenanceDate' => Carbon::parse($this->maintenance_time)->format('Y-m-d'),
            'maintenanceTime' => (Carbon::parse($this->maintenance_time)->format('m.d.')) . ($this->getMaintenanceType() === 2 ? ' Ü' : ' A'),
            'note' => $this->note ?: '',
            'createdAt' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
        ];
    }
}
