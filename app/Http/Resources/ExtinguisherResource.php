<?php

namespace App\Http\Resources;

use App\Models\Extinguisher;
use App\Models\Maintenance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ExtinguisherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        $productionTime = Carbon::parse($this->production_time);

        return [
            'id' => $this->id,
            'identifier' => $this->identifier,
            'premises' => $this->premises,
            'location' => $this->location,
            'extinguisherType' => $this->extinguisher_type,
            'extinguisherTypeText' => Extinguisher::TYPES[$this->extinguisher_type]['value'],
            'serialNumber' => $this->serial_number,
            'productionTime' => [
                'year' => (int) $productionTime->format('Y'),
                'month' => (int) $productionTime->format('n'),
                'day' => (int) $productionTime->format('j'),
            ],
            'productionTimeStr' => $productionTime->format('Y.m.d.'),
            'note' => $this->note ?: '',
            'hasBasicMaintenanceType' => $this->hasBasicMaintenanceType(),
            'operatorsMaintenanceType' => $this->getOperatorsMaintenanceType(),
            'hasMaintenance' => $this->hasMaintenance(),
            'maintenanceTime' => $this->getBasicMaintenance() ? Carbon::parse($this->getBasicMaintenance()->maintenance_time)->format('m.d.') . ' A' : null,
            'maintenances' => $this->hasMaintenance() ? MaintenanceResource::collection($this->maintenances()->get()) : [],
            'basicMaintenance' => $this->getBasicMaintenance() ? new MaintenanceResource($this->getBasicMaintenance()) : null,
            'firstMaintenance' => $this->getFirstMaintenance() ? new MaintenanceResource($this->getFirstMaintenance()) : null,
            'secondMaintenance' => $this->getSecondMaintenance() ? new MaintenanceResource($this->getSecondMaintenance()) : null,
            'thirdMaintenance' => $this->getThirdMaintenance() ? new MaintenanceResource($this->getThirdMaintenance()) : null,
            'fourthMaintenance' => $this->getFourthMaintenance() ? new MaintenanceResource($this->getFourthMaintenance()) : null,
            'lastMaintenance' => $this->maintenances()->orderBy('maintenance_period', 'desc')->first() ? new MaintenanceResource($this->maintenances()->orderBy('maintenance_period', 'desc')->first()) : null,
            'createdAt' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
        ];
    }
}
