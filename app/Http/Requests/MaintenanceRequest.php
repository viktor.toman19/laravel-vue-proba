<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rules\Password;
use Symfony\Component\HttpFoundation\Response;

class MaintenanceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'extinguisherId' => 'required|integer|exists:extinguishers,id',
            'maintenanceDate' => 'required|date',
            'note' => 'nullable|string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validation errors',
            'errors' => $validator->errors(),
        ], Response::HTTP_BAD_REQUEST));
    }
}
