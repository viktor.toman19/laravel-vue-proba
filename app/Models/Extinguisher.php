<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Extinguisher extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'extinguishers';
    protected $guarded = ['id'];
    public const TYPES =
        [[
            'key' => 0,
            'value' => 'n/a',
        ], [
            'key' => 1,
            'value' => 'Porral oltó készülék',
        ], [
            'key' => 2,
            'value' => 'Anafgroup PS1-x (5 A)',
        ]];

    public function maintenances(): HasMany
    {
        return $this->hasMany(Maintenance::class);
    }

    public function hasBasicMaintenanceType(): bool
    {
        return $this->maintenances()->where('maintenance_type', Maintenance::TYPE_BASIC)->count() > 0;
    }

    public function getBasicMaintenance(): Model|null
    {
        return $this->maintenances()->where('maintenance_type', Maintenance::TYPE_BASIC)->first();
    }

    public function getOperatorsMaintenanceType(): int
    {
        return $this->maintenances()->where('maintenance_type', Maintenance::TYPE_OPERATORS)->count();
    }

    public function hasMaintenance(): bool
    {
        return $this->maintenances()->count() > 0;
    }

    public function getFirstMaintenance(): Model|null
    {
        return $this->maintenances()->where('maintenance_period', 1)->first();
    }

    public function getSecondMaintenance(): Model|null
    {
        return $this->maintenances()->where('maintenance_period', 2)->first();
    }

    public function getThirdMaintenance(): Model|null
    {
        return $this->maintenances()->where('maintenance_period', 3)->first();
    }

    public function getFourthMaintenance(): Model|null
    {
        return $this->maintenances()->where('maintenance_period', 4)->first();
    }
}
