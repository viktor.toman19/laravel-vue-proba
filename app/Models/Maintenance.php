<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Maintenance extends Model
{
    use HasFactory, SoftDeletes;

    public const TYPE_BASIC = 'basic';
    public const TYPE_OPERATORS = 'operators';

    protected $table = 'maintenances';
    protected $guarded = ['id'];

    public function extinguisher(): BelongsTo
    {
        return $this->belongsTo(Extinguisher::class);
    }

    public function getMaintenanceType(): int
    {
        $extinguisher = $this->extinguisher()->first();

        if (!$extinguisher->hasMaintenance()) {
            return 1;
        } elseif ($extinguisher->getBasicMaintenance()) {
            return 2;
        } else {
            return 1;
        }
    }
}
