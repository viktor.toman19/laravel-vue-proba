<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extinguishers', function (Blueprint $table) {
            $table->id();

            $table->integer('premises')->nullable();
            $table->string('identifier')->nullable();
            $table->string('location')->nullable();
            $table->integer('extinguisher_type')->nullable();
            $table->string('serial_number')->nullable();
            $table->timestamp('production_time')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('extinguishers');
    }
};
