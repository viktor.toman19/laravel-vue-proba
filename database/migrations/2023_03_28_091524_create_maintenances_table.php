<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maintenances', function (Blueprint $table) {
            $table->id();

            $table->enum('maintenance_type', ['basic', 'operators'])->nullable();
            $table->integer('maintenance_period');
            $table->timestamp('maintenance_time')->nullable();
            $table->text('note')->nullable();

            $table->unsignedBigInteger('extinguisher_id')->nullable();
            $table->foreign('extinguisher_id')->references('id')->on('extinguishers');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maintenances');
    }
};
